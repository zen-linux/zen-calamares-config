#!/usr/bin/sh

exec pkexec \
  --disable-internal-agent \
  env \
    DISPLAY="$DISPLAY" \
    QT_QPA_PLATFORM="$QT_QPA_PLATFORM" \
    WAYLAND_DISPLAY="$WAYLAND_DISPLAY" \
    XDG_RUNTIME_DIR="$XDG_RUNTIME_DIR" \
    QT_QPA_PLATFORMTHEME=kde \
  /usr/bin/calamares -d
