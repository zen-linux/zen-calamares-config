#!/usr/bin/sh

TARGET=$1

echo "bootstrapping a zen linux installation to ${TARGET}"

PACMAN_CONFIG=$(mktemp)
printf "[zen-core]\nServer = https://repos.zenlinux.net/zen/core\n" >> ${PACMAN_CONFIG}
#printf "[zen-test]\nServer = https://repos.zenlinux.net/zen/test\n" >> ${PACMAN_CONFIG}

mkdir -p ${TARGET}/var/lib/pacman
mkdir -p ${TARGET}/var/cache/pacman/pkg

pacman --root "${TARGET}" --config "${PACMAN_CONFIG}" --noconfirm -Sy \
  zen-desktop
