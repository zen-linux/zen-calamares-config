
DESTDIR = "/"

install: settings.conf module-settings module-scripts brandings launcher-script

settings.conf:
	mkdir -pv "$(DESTDIR)/usr/share/calamares"
	cp -v settings/settings.conf $(DESTDIR)/usr/share/calamares/settings.conf

module-settings:
	mkdir -pv "$(DESTDIR)/usr/share/calamares/modules"
	cp -v settings/modules/* $(DESTDIR)/usr/share/calamares/modules/

module-scripts:
	mkdir -pv "$(DESTDIR)/usr/share/calamares/module-scripts"
	cp -v scripts/pacman-bootstrap.sh $(DESTDIR)/usr/share/calamares/module-scripts/pacman-bootstrap.sh

brandings:
	mkdir -pv "$(DESTDIR)/usr/share/calamares/branding"
	cp -Rv brandings/* "$(DESTDIR)/usr/share/calamares/branding/"

launcher-script:
	mkdir -pv "$(DESTDIR)/usr/bin"
	cp -v scripts/zen-linux-installer.sh "$(DESTDIR)/usr/bin/zen-linux-installer"

.PHONY: install settings.conf modules-settings module-scripts brandings launcher-script
